package org.equinox.factory;

import org.adempiere.base.IProcessFactory;
import org.compiere.process.ProcessCall;
import org.equinox.process.RevertPayment;

public class RevertPaymentFactory implements IProcessFactory {

	@Override
	public ProcessCall newProcessInstance(String className) {
		// TODO Auto-generated method stub
		if(className.equals("org.equinox.process.RevertPayment"))
			return new RevertPayment();
		
		return null;
	}

}
